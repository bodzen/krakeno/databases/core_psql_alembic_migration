#!/usr/bin/python3
# -*- coding: utf-8 -*-

from sqlalchemy import Column, String, Integer
from sqlalchemy.ext.declarative import declarative_base

__all__ = ['BaseInvoiceStatus', 'InvoiceStatus']

BaseInvoiceStatus = declarative_base()


class InvoiceStatus(BaseInvoiceStatus):
    __tablename__ = 'invoice_status'
    id = Column(Integer, primary_key=True, unique=True)
    name = Column(String(50))

    def __repr__(self):
        return f'<Invoice type>\n' \
            f'	id: {self.id}\n' \
            f'	name: {self.name}'
