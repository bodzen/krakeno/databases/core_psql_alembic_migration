#!/usr/bin/python3
# -*- coding: utf-8 -*-

from sqlalchemy import Column, String, Integer, ForeignKey, DateTime
from sqlalchemy.ext.declarative import declarative_base
from models.users import Users


__all__ = ['BaseUserCards', 'UserCards']

BaseUserCards = declarative_base()


class UserCards(BaseUserCards):
    __tablename__ = 'user_cards'
    user_id = Column(Integer, ForeignKey(Users.id, name='fk_user_id'), primary_key=True)
    fingerprint = Column(String(35))
    last_transaction_date = Column(DateTime())  # datetime.datetime.now(tz=pytz.timezone("Europe/Paris"))
    first_transaction_date = Column(DateTime())  # datetime.datetime.now(tz=pytz.timezone("Europe/Paris"))
    country = Column(String(5))

    def __repr__(self):
        return '<User Card>\n' \
            f'	user_id: {self.user_id}\n' \
            f'	fingerprint: {self.fingerprint}\n' \
            f'	last_transaction_date: {self.last_transaction_date}\n' \
            f'	first_transaction_date: {self.first_transaction_date}' \
            f'	country: {self.country}'
