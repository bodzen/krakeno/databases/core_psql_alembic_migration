#!/usr/bin/python3
# -*- coding: utf-8 -*-

from sqlalchemy import Column, String, Integer
from sqlalchemy.ext.declarative import declarative_base

__all__ = ['BaseSubTypes', 'SubTypes']

BaseSubTypes = declarative_base()


class SubTypes(BaseSubTypes):
    __tablename__ = 'subscription_types'
    id = Column(Integer, primary_key=True, unique=True)
    sub_type = Column(String(50))

    def __repr__(self):
        return '<Subscription Type>\n' \
            f'	id: {self.id}\n' \
            f'	sub_type: {self.sub_type}'
