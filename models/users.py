#!/usr/bin/python3
# -*- coding: utf-8 -*-

from sqlalchemy import Column, String, Integer
from sqlalchemy.ext.declarative import declarative_base


__all__ = ['BaseUsers', 'Users']
BaseUsers = declarative_base()


class Users(BaseUsers):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True, unique=True)
    email = Column(String(120), index=True, unique=True)
    password = Column(String(128))
    ip = Column(String(120), index=True, unique=True)
    status = Column(String(25), unique=False)
    customer_id = Column(String(30), index=True, unique=True)

    def __repr__(self):
        return '<User>:\n' \
            f'	user_id: {self.id}\n' \
            f'	email: {self.email}\n' \
            f'	ip: {self.ip}\n' \
            f'	status: {self.status}\n' \
            f'	customer_id: {self.customer_id}'
