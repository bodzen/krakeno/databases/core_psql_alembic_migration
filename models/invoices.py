#!/usr/bin/python3
# -*- coding: utf-8 -*-

from sqlalchemy import Column, String, Integer, ForeignKey, Boolean
from sqlalchemy.ext.declarative import declarative_base
from models.users import Users
from models.invoice_status import InvoiceStatus


__all__ = ['BaseInvoices', 'Invoices']

BaseInvoices = declarative_base()


class Invoices(BaseInvoices):
    __tablename__ = 'invoices'
    id = Column(Integer, primary_key=True, unique=True)
    user_id = Column(Integer, ForeignKey(Users.id, name='fk_user_id'), primary_key=True)
    paid = Column(Boolean)
    status = Column(Integer, ForeignKey(InvoiceStatus.id, name='fk_invoice_type_id'))
    invoice_url = Column(String(256))

    def __repr__(self):
        return '<Invoice>\n' \
            f'	id: {self.id}\n' \
            f'	paid: {self.id}\n' \
            f'	status: {self.status}\n' \
            f'	sub_id: {self.sub_id}\n' \
            f'	invoice_url: {self.recipe_url}'
