#!/usr/bin/python3
# -*- coding: utf-8 -*-

from sqlalchemy import Column, String, Integer
from sqlalchemy.ext.declarative import declarative_base

__all__ = ['BaseSubStatus', 'SubStatus']

BaseSubStatus = declarative_base()


class SubStatus(BaseSubStatus):
    __tablename__ = 'subscription_status'
    id = Column(Integer, primary_key=True, unique=True)
    name = Column(String(50))

    def __repr__(self):
        return f'<Subscription status>\n' \
            f'	id: {self.id}\n' \
            f'	name: {self.name}'
