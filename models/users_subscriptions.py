#!/usr/bin/python3
# -*- coding: utf-8 -*-

from sqlalchemy import Column, String, Integer, ForeignKey, DateTime
from sqlalchemy.ext.declarative import declarative_base

from models.users import Users
from models.subscription_types import SubTypes
from models.subscription_status import SubStatus
from models.invoices import Invoices


__all__ = ['BaseUsersSubs', 'UsersSubs']

BaseUsersSubs = declarative_base()


class UsersSubs(BaseUsersSubs):
    __tablename__ = 'users_subscriptions'
    id = Column(String(128), primary_key=True, unique=True)
    user_id = Column(Integer, ForeignKey(Users.id, name='fk_user_id'), primary_key=True)
    sub_type_id = Column(Integer, ForeignKey(SubTypes.id, name='fk_sub_type_id'), primary_key=True)
    status = Column(Integer, ForeignKey(SubStatus.id, name='fk_sub_status'), primary_key=True)
    invoice_id = Column(Integer, ForeignKey(Invoices.id, name='fk_invoices_id'), primary_key=True)
    receipt_url = Column(String(256))
    date_start = Column(DateTime())
    date_end = Column(DateTime())

    def __repr__(self):
        return f'<User Subscription>\n' \
            f'	id: {self.id}\n' \
            f'	user_id: {self.user_id}\n' \
            f'	sub_type_id: {self.sub_type_id}\n' \
            f'	satus: {self.status}\n' \
            f'	invoice_id: {self.invoice_id}\n' \
            f'	receipt_url: {self.receipt_url}\n' \
            f'	date_start: {self.date_start}\n' \
            f'	date_end: {self.date_end}'
