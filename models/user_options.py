#!/usr/bin/python3
# -*- coding: utf-8 -*-

from sqlalchemy import Column, Boolean, Integer, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from models.users import Users


__all__ = ['BaseUserOptions', 'UserOptions']
BaseUserOptions = declarative_base()


class UserOptions(BaseUserOptions):
    __tablename__ = 'user_options'
    user_id = Column(Integer, ForeignKey(Users.id, name='fk_user_id'), primary_key=True)
    privacy_policy = Column(Boolean, unique=False, default=False)
    tos = Column(Boolean, unique=False, default=False)
    sla = Column(Boolean, unique=False, default=False)

    def __repr__(self):
        return '<user_option>\n' \
            f'	user_id:{self.user_id}\n' \
            f'	Privacy policy: {self.privacy_policy}\n' \
            f'	Terms of use: {self.tos}\n' \
            f'	Service Level Agreement: {self.sla}'
