# db-krakeno

How to update database schema using alembic

1. Create or modify models in ./models folder
2. Let alembic auto-generate a migration script
```
export PYTHONPATH=$PWD
alembic revision --autogenerate -m "SOME COMMIT MESSAGE"
```
3. Let alembic upgrade the schema
```
alembic upgrade head
```
