"""Add receipt_url + invoice_url

Revision ID: 211bfe17f9ef
Revises: a5ea97ea6f7a
Create Date: 2020-07-23 14:08:26.448248

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '211bfe17f9ef'
down_revision = 'a5ea97ea6f7a'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('invoices', sa.Column('invoice_url', sa.String(length=256), nullable=True))
    op.drop_column('invoices', 'recipe_url')
    op.add_column('user_cards', sa.Column('country', sa.String(length=5), nullable=True))
    op.add_column('users_subscriptions', sa.Column('receipt_url', sa.String(length=256), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('users_subscriptions', 'receipt_url')
    op.drop_column('user_cards', 'country')
    op.add_column('invoices', sa.Column('recipe_url', sa.VARCHAR(length=256), autoincrement=False, nullable=True))
    op.drop_column('invoices', 'invoice_url')
    # ### end Alembic commands ###
