"""baseline

Revision ID: ce009fd03961
Revises:
Create Date: 2020-01-08 14:28:44.761592

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ce009fd03961'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'users',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('email', sa.String(120), index=True, unique=True),
        sa.Column('password', sa.String(128))
    )


def downgrade():
    op.drop_table('users')
